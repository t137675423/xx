package socket

import "bytes"

var routers [100]Router

type Router struct {
	//上级路由
	superior *Router
	//路由路径
	path []byte
	//路由前置函数
	hooks []Hook
	//路由处理函数
	handle Hook
}

//findEmptyRouter 寻找一个空路由
func findEmptyRouter() *Router {
	for k := range routers {
		if routers[k].path == nil {
			return &routers[k]
		}
	}
	return nil
}

//findExistRouter 寻找一个已有路由
func findExistRouter(path []byte) *Router {
	for k := range routers {
		if bytes.Equal(routers[k].path, path) {
			return &routers[k]
		}
	}
	return nil
}

func (r *Router) Set(h Hook) {
	r.handle = h
}

func (r *Router) Group(path []byte) *Router {
	item := findEmptyRouter()
	item.superior = r
	item.path = path
	return item
}

func (r *Router) Add(path []byte, h Hook) {
	item := findEmptyRouter()
	item.superior = r
	item.path = path
	item.handle = h
}

func (r *Router) Use(h Hook) {
	r.hooks = append(r.hooks, h)
}

//FindExistRouter 寻找一个已有路由
func FindExistRouter(path []byte) *Router {
	return findExistRouter(path)
}

//Exec 执行路由方法
func (r *Router) Exec(s *Stream) {
	r.before(s)
	if r.handle != nil {
		r.handle(s)
	}
}

func (r *Router) before(s *Stream) {
	if r.superior != nil {
		r.superior.before(s)
	}
	for _, h := range r.hooks {
		h(s)
	}
}
