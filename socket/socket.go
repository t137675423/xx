package socket

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net"
)

type Server struct {
	//服务运行状态
	running bool
	//缓冲区
	buffer int
}

func (socket *Server) Init() {
	if socket.buffer == 0 {
		socket.buffer = 128
	}

}

func (socket *Server) SetBuffLen(l int) {
	socket.buffer = l
}

func (socket *Server) Group(path []byte) *Router {
	router := findEmptyRouter()
	router.path = path
	return router
}

func (socket *Server) Run(port int) {
	if socket.running {
		return
	}

	socket.running = true

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go socket.handle(conn)
	}
}

func (socket *Server) handle(conn net.Conn) {
	s := Stream{}
	s.conn = conn
	buff := make([]byte, socket.buffer)
	var last []byte
	s.init()
	var n int
	var err error
	for {
		select {
		case <-s.close: //关闭连接
			s.Track(errors.New("bye"))
			s.End()
			return
		case <-s.resend: //重新处理
		default: //新数据包
			n, err = s.conn.Read(buff)
			if err != nil {
				s.Track(err)
				s.End()
				return
			}
		}

		last = append(last, buff[:n]...)

		//数据包定界符为\r\n
		bs := bytes.Split(last, []byte{13, 10})

		// 还没凑足了一个整包
		if len(bs) < 2 {
			continue
		}

		for k := range bs {
			if k < len(bs)-1 {
				if routers[0].handle != nil {
					s.body = bs[k]
					routers[0].handle(&s)
				}
			}
		}
		last = bs[len(bs)-1]
	}
}
