package socket

import (
	"log"
	"net"
)

type Stream struct {
	conn   net.Conn
	resend chan struct{}
	close  chan struct{}
	body   []byte
	track  []interface{}
}

func (s *Stream) init() {
	s.resend = make(chan struct{}, 1)
	s.close = make(chan struct{}, 1)
}

func (s *Stream) ReSend() {
	s.resend <- struct{}{}
}

func (s *Stream) GetBody() []byte {
	return s.body
}

func (s *Stream) Close() {
	s.Track(s.conn.Close())
	s.close <- struct{}{}
}

func (s *Stream) Track(info interface{}) {
	s.track = append(s.track, info)
}

func (s *Stream) End() {
	log.Println(s.track)
}

func (s *Stream) Rsp(data []byte) (int, error) {
	return s.conn.Write(data)
}
