package web

import (
	"fmt"
	"strings"
)

var routers [1024]Router

type Router struct {
	//上级路由
	superior *Router
	//路由路径
	path string
	//路由前置函数
	hooks []Hook
	//路由处理函数
	handle Hook
}

//findEmptyRouter 寻找一个空路由
func findEmptyRouter() *Router {
	for k := range routers {
		if routers[k].path == "" {
			return &routers[k]
		}
	}
	return nil
}

//findExistRouter 寻找一个已有路由
func findExistRouter(path string) *Router {
	p := strings.Split(path, "/")

	for k := range routers {
		if strings.Trim(routers[k].path, "/") == strings.ToUpper(strings.Join(p, "")) {
			return &routers[k]
		}
	}
	return nil
}

func (r *Router) Group(path string) *Router {
	item := findEmptyRouter()
	item.superior = r
	item.path = strings.ToUpper(fmt.Sprintf("%s%s", r.path, path))
	return item
}

func (r *Router) Add(path string, h Hook) {
	item := findEmptyRouter()
	item.superior = r
	item.path = strings.ToUpper(fmt.Sprintf("%s%s", r.path, path))
	item.handle = h
}

func (r *Router) GP(path string, h Hook) {
	item := findEmptyRouter()
	item.superior = r
	item.path = strings.ToUpper(fmt.Sprintf("%s%s", r.path, path))
	item.handle = h
}

func (r *Router) Use(h Hook) {
	r.hooks = append(r.hooks, h)
}

func (r *Router) before(s *Stream) {
	if r.superior != nil {
		r.superior.before(s)
	}
	for _, h := range r.hooks {
		h(s)
	}
}
