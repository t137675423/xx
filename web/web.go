package web

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

type Server struct {
	//服务运行状态
	running bool
}

func (www *Server) Init() {

}

func (www *Server) Run(port int) {
	if www.running {
		return
	}

	www.running = true

	server := &http.Server{
		Addr:         fmt.Sprintf(":%d", port),
		WriteTimeout: time.Second * 30, //设置3秒的写超时
		Handler:      www,
	}

	log.Fatal(server.ListenAndServe())
}

func (www *Server) RunHTTPS(domain ,cert,key string) {
	if www.running {
		return
	}

	www.running = true
	server := &http.Server{
		Addr:         fmt.Sprintf("%s", domain),
		WriteTimeout: time.Second * 30, //设置3秒的写超时
		Handler:      www,
	}

	log.Fatal(server.ListenAndServeTLS(cert,key))
}

func (www *Server) Group(path string) *Router {
	router := findEmptyRouter()
	router.path = strings.ToUpper(path)
	return router
}

func (www *Server) ShowRouter() {
	for k := range routers {
		if routers[k].path != "" {
			fmt.Println(fmt.Sprintf("[%s] ", routers[k].path))
		}
	}
}

func (www *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//查询路由
	router := findExistRouter(r.URL.Path)
	if router == nil {
        w.WriteHeader(404)
		w.Write([]byte("page not found"))
		return
	}

	//每个请求都生成一个对象,途径路线日志全写入此日志对象
	s := Stream{}
	s.r, s.w = r, w
	s.Log(s.r.URL.Path)
	router.before(&s)
	if len(s.ls) > 1 {
		return
	}
	if router.handle != nil {
		router.handle(&s)
	}
}
