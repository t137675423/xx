package xx

import (
	"gitee.com/t137675423/xx/log"
	"gitee.com/t137675423/xx/socket"
	"gitee.com/t137675423/xx/web"
)

var body Server

func New() *Server {
	if !body.initialize {
		body.init()
	}
	return &body
}

type Server struct {
	initialize bool

	socket socket.Server
	web    web.Server
	log    log.Server
}

func (s *Server) init() {
	s.initialize = true
	s.web.Init()
	s.socket.Init()
}

func (s *Server) WebRun(port int) error {
	s.web.ShowRouter()
	go s.web.Run(port)
	return nil
}

func (s *Server) WebGroup(path string) *web.Router {
	return s.web.Group(path)
}

func (s *Server) WebRunHTTPS(domain,cert,key string) error {
	s.web.ShowRouter()
	go s.web.RunHTTPS(domain,cert,key)
	return nil
}

func (s *Server) SocketRun(port int) error {
	go s.socket.Run(port)
	return nil
}

func (s *Server) SocketGroup(path []byte) *socket.Router {
	return s.socket.Group(path)
}

func (s *Server) SocketSetBuffLen(l int) {
	s.socket.SetBuffLen(l)
}
