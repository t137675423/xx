package xx

import (
	"fmt"
	"gitee.com/t137675423/xx/socket"
	"gitee.com/t137675423/xx/web"
	"testing"
	"time"
)

type req struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func webHandle(s *web.Stream) {
	var p req
	s.BinParam(&p)
	fmt.Println(p)
}

func socketHandle(s *socket.Stream) {
	_, err := s.Rsp(s.GetBody())
	if err != nil {
		s.Close()
		return
	}
	time.Sleep(time.Second * 2)
	s.ReSend()
}

func Test(t *testing.T) {
	app := New()
	fmt.Println(app.WebRun(7000))
	wwwRoot := app.WebGroup("/")
	wwwRoot.Add("my", webHandle)

	time.Sleep(time.Hour)

}
